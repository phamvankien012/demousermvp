package com.hlub.dev.demousermvp.adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.adapter.holder.LoadingViewHolder;
import com.hlub.dev.demousermvp.adapter.holder.NetworkViewHolder;
import com.hlub.dev.demousermvp.adapter.holder.UserViewHolder;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.screen.list_user.presenter.ListUserPresenter;
import com.hlub.dev.demousermvp.screen.list_user.view.ListUserView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Datum> datumList = new ArrayList<>();
    private Context context;
    private ListUserView view;

    private boolean isLoadMore=true;
    private boolean isNetwork;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_NETWORK = 2;


    public boolean isLoadMore() {
        return isLoadMore;
    }

    public void setNetwork(boolean isNetwork) {
        this.isNetwork = isNetwork;
    }

    public UserAdapter(Context context, ListUserView view) {
        this.context = context;
        this.view = view;
    }

    public void setListData(List<Datum> datumModels) {
        isLoadMore = true;
        datumList.clear();
        datumList.addAll(datumModels);
        notifyDataSetChanged();
        Log.e("user", "setListData: " + isLoadMore);
    }

    public void addData(List<Datum> list) {
        notifyItemRemoved(datumList.size());//xóa loading trước đó ở listsize+1
        datumList.addAll(list);
        notifyItemRangeInserted(datumList.size() - list.size(), getItemCount());
        Log.e("user", "addData: " + isLoadMore);
        isLoadMore = true;//set lại loading

//        if (datumList.addAll(list)) {
//            notifyItemRemoved(datumList.size());//xóa loading trước đó ở listsize+1
//            isLoadMore = false;
//        } else {
//            datumList.addAll(list);
//            notifyItemRangeInserted(datumList.size() - list.size(), getItemCount());
//        }
//        isLoadMore = true;//set lại loading

//        datumList.add(null);
//        isLoadMore = true;
//        notifyItemInserted(datumList.size() - 1);
    }

    public void removeNull() {
        isLoadMore = false;
        notifyItemRemoved(datumList.size());
        Log.e("user", "removeNull: " + datumList.size()+" - "+isLoadMore);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }

    public boolean isDataEmpty() {
        return datumList.isEmpty();
    }

    public void deleteUser(final int position) {
        datumList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, datumList.size());

    }

    public void updateItem(int pos, String name, String job) {
        datumList.get(pos).setFirstName(name);
        datumList.get(pos).setLastName("");
        datumList.get(pos).setEmail(job);
        notifyItemChanged(pos);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
            return new UserViewHolder(view, context);

        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);

        } else if (viewType == VIEW_TYPE_NETWORK) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_no_network, parent, false);
            return new NetworkViewHolder(view);
        } else {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (!datumList.isEmpty()) {
            if (holder instanceof UserViewHolder) {
                Datum datum = datumList.get(position);
                UserViewHolder userViewHolder = (UserViewHolder) holder;
                userViewHolder.bind(view, datum, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (!isNetwork && datumList.isEmpty()) {
            return 1;
        } else {
            return isLoadMore ? (datumList.size() + 1) : datumList.size() ;
        }

    }

    public int getItemViewType(int position) {
        if (!isNetwork) {
            return VIEW_TYPE_NETWORK;
        } else {
            if(isLoadMore){
                if (position < datumList.size()) {
                    return VIEW_TYPE_ITEM;
                } else {
                    return VIEW_TYPE_LOADING;
                }
            }else{
                return VIEW_TYPE_ITEM;
            }



        }
    }


}
