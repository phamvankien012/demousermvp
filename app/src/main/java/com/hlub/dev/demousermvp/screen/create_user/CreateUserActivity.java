package com.hlub.dev.demousermvp.screen.create_user;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextWatcher;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.Toast;
import android.content.Intent;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.base.BaseActivity;
import com.hlub.dev.demousermvp.screen.create_user.presenter.CreateUserPresenter;
import com.hlub.dev.demousermvp.screen.create_user.view.ICreateUserView;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;
import com.hlub.dev.demousermvp.utils.KeyboardHelper;

public class CreateUserActivity extends BaseActivity implements ICreateUserView {
    private TextInputEditText edtName;
    private TextInputEditText edtJob;
    private TextInputLayout InputLayoutName;
    private TextInputLayout InputLayoutJob;
    private ImageView imgBackFromCreate;
    private Button btnCreate;

    private CreateUserPresenter presenter;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        presenter = new CreateUserPresenter(this);
        progressDialog = new ProgressDialog(this);
        initView();
        initEditTextChange();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyboardHelper.showKeyboard(getContext());
    }

    @Override
    protected void onStop() {
        super.onStop();
        progressDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }

    private void initView() {
        edtName = findViewById(R.id.edtName);
        edtJob = findViewById(R.id.edtJob);
        InputLayoutName = findViewById(R.id.InputLayoutName);
        InputLayoutJob = findViewById(R.id.InputLayoutJob);
        imgBackFromCreate = findViewById(R.id.imgBackFromCreate);
        btnCreate = findViewById(R.id.btnCreate);

    }

    private void initEditTextChange() {
        //NAME
        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 0) {
                    InputLayoutName.setError("Tên không được rỗng");
                    InputLayoutName.requestFocus();
                } else
                    InputLayoutName.setError(null);

            }
        });

        //JOB
        edtJob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 0)
                    InputLayoutJob.setError("Nghề nghiệp không được rỗng");
                else
                    InputLayoutJob.setError(null);
            }
        });

    }

    private void initListener() {
        imgBackFromCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });
    }


    private void createUser() {
        final String name = edtName.getText().toString();
        final String job = edtJob.getText().toString();

        presenter.createUser(progressDialog, CreateUserActivity.this, name, job);

        if (!name.isEmpty() && !job.isEmpty()) {
            KeyboardHelper.closeKeyboard(this, this);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        KeyboardHelper.closeKeyboard(getContext(), this);
    }

    /*
        PRESENTER
         */
    @Override
    public void onCreateUserSuccess() {
        progressDialog.dismiss();
        if (!progressDialog.isShowing()) {
            finish();
        }
        showToastShort("Tạo user thành công");

    }

    @Override
    public void onCreateUserError(String error) {
        showToastShort(error);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onValidateName(String error) {
        InputLayoutName.setError(error);
        InputLayoutName.requestFocus();
        KeyboardHelper.showKeyboard(getContext());
    }

    @Override
    public void onValidateJob(String error) {
        InputLayoutJob.setError(error);
        InputLayoutJob.requestFocus();
        KeyboardHelper.showKeyboard(getContext());
    }

    @Override
    public void onShowLoading(String mess) {
        DialogHelper.dialogLoading(progressDialog, mess);
    }

    @Override
    public void onCloseLoading() {
        progressDialog.dismiss();
    }

}
