package com.hlub.dev.demousermvp.utils;

public interface DialogCallback {
    void onAgree();

    void disAgree();
}
