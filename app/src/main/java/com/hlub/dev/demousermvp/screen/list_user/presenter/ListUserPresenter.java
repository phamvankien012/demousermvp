package com.hlub.dev.demousermvp.screen.list_user.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.entity.User;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.screen.list_user.model.ListUserInteractor;
import com.hlub.dev.demousermvp.screen.list_user.view.ListUserView;
import com.hlub.dev.demousermvp.utils.NetworkHelper;


public class ListUserPresenter {
    private ListUserInteractor listUserInteractor = new ListUserInteractor();

    private ListUserView listUserView;

    public ListUserPresenter(ListUserView listUserView) {
        this.listUserView = listUserView;
    }


    public void getListUserByPage(int page, int per_page) {
        if (!NetworkHelper.isCheckNetwork(listUserView.getContext())) {
            listUserView.onNetworkNotConnected();
            return;
        }
        listUserInteractor.getListUserByPage(page, per_page, new RetrofitCallback<User>() {
            @Override
            public void onSuccess(User obj) {
                listUserView.onGetListDatumSuccess(obj.getData());
            }

            @Override
            public void onError(String error) {
                listUserView.onGetListUserError(error);
            }
        });
    }

    public void cancelRequestServer() {
        listUserInteractor.cancelListUserByPage();
        Log.e("cancelRequestServer", "cancelRequestServer");
    }


    public void deleteUser(final int id, final int position) {
        if (!NetworkHelper.isCheckNetwork(listUserView.getContext())) {
            listUserView.onNetworkNotConnected();
            return;
        }

        listUserView.onShowLoadingDeleteUser("Please wait while deleting...");

        listUserInteractor.deleteUser(id, new RetrofitCallback<User>() {
            @Override
            public void onSuccess(User obj) {
                listUserView.onDeleteUserSuccess(position);
                listUserView.onCloseLoadingDelete();
            }

            @Override
            public void onError(String error) {
                listUserView.onCloseLoadingDelete();
                listUserView.onDeleteUserError("Xóa User không thành công");
            }
        });
    }


}
