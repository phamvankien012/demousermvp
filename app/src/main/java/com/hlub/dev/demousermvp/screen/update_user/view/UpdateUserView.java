package com.hlub.dev.demousermvp.screen.update_user.view;

import android.content.Context;

public interface UpdateUserView {


    void onUpdateUserSuccess();

    void onUpdateUserError(String error);

    void onValidateName(String error);

    void onValidateJob(String error);

    Context getContext();

    void onShowLoading(String mess);

    void onCloseLoading();

}
