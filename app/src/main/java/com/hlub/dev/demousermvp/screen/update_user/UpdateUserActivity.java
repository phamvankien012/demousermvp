package com.hlub.dev.demousermvp.screen.update_user;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.content.Intent;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.base.BaseActivity;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.screen.list_user.ListUserActivity;
import com.hlub.dev.demousermvp.screen.update_user.presenter.UpdateUserPresenter;
import com.hlub.dev.demousermvp.screen.update_user.view.UpdateUserView;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;
import com.hlub.dev.demousermvp.utils.KeyboardHelper;

public class UpdateUserActivity extends BaseActivity implements UpdateUserView {

    private ImageView imgBackFromUpdate;
    private TextInputLayout inputLayoutName;
    private TextInputEditText edtName;
    private TextInputLayout inputLayoutJob;
    private TextInputEditText edtJob;
    private Button btUpdate;

    private UpdateUserPresenter presenter;

    private int id, pos;
    private String name, job;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        presenter = new UpdateUserPresenter(this);
        progressDialog = new ProgressDialog(this);

        initView();
        getDataFromListActivity();
        setEditText();
        initEditTextChange();
        initListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyboardHelper.showKeyboard(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        progressDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
    }

    private void initView() {
        imgBackFromUpdate = findViewById(R.id.imgBackFromUpdate);
        inputLayoutName = findViewById(R.id.inputLayoutName);
        edtName = findViewById(R.id.edtName);
        inputLayoutJob = findViewById(R.id.inputLayoutJob);
        edtJob = findViewById(R.id.edtJob);
        btUpdate = findViewById(R.id.btUpdate);
    }

    private void getDataFromListActivity() {
        Datum datum = (Datum) getIntent().getSerializableExtra("datum");
        id = datum.getId();
        name = datum.getFirstName() + " " + datum.getLastName();
        pos = Integer.parseInt(getIntent().getStringExtra("pos"));
        if (datum.getEmail().indexOf("@") != -1) {
            job = datum.getEmail().substring(0, datum.getEmail().indexOf("@"));
        } else {
            job = datum.getEmail();
        }
    }

    private void setEditText() {
        edtName.setText(name);
        edtJob.setText(job);
    }


    private void initEditTextChange() {
        //NAME
        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 0) {
                    inputLayoutName.setError("Tên không được rỗng");
                    inputLayoutName.requestFocus();
                } else
                    inputLayoutName.setError(null);

            }
        });

        //JOB
        edtJob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 0)
                    inputLayoutJob.setError("Nghề nghiệp không được rỗng");
                else
                    inputLayoutJob.setError(null);
            }
        });

    }

    private void initListener() {
        imgBackFromUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });
    }

    public void updateUser() {
        name = edtName.getText().toString();
        job = edtJob.getText().toString();

        presenter.updateUser(progressDialog, UpdateUserActivity.this, id, name, job);
        if (!name.isEmpty() && !job.isEmpty()) {
            KeyboardHelper.closeKeyboard(this, this);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK,new Intent());
        finish();
        KeyboardHelper.closeKeyboard(this, this);
    }


    /*
     *PRESENTER
     */

    @Override
    public void onUpdateUserSuccess() {
        progressDialog.dismiss();
        Intent data = new Intent();
        data.putExtra("pos", String.valueOf(pos));
        data.putExtra("name", name);
        data.putExtra("email", job);
        setResult(RESULT_OK, data);
        finish();
    }
    @Override
    public void onUpdateUserError(String error) {
        showToastShort(error);
    }

    @Override
    public void onValidateName(String error) {
        inputLayoutName.setError(error);
        inputLayoutName.requestFocus();
        KeyboardHelper.showKeyboard(this);
    }

    @Override
    public void onValidateJob(String error) {
        inputLayoutJob.setError(error);
        inputLayoutJob.requestFocus();
        KeyboardHelper.showKeyboard(this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onShowLoading(String mess) {
        DialogHelper.dialogLoading(progressDialog,mess);
    }

    @Override
    public void onCloseLoading() {
progressDialog.dismiss();
    }


}
