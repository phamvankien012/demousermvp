package com.hlub.dev.demousermvp.base;

import android.content.Context;
import android.widget.Toast;

import com.hlub.dev.demousermvp.utils.NetworkHelper;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    public void showToastShort(String mess) {
        Toast.makeText(this, mess, Toast.LENGTH_SHORT).show();
    }

    public boolean checkOnNetwork(Context context) {
        return NetworkHelper.isCheckNetwork(context);
    }
}
