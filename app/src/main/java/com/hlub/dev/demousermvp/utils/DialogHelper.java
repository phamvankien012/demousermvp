package com.hlub.dev.demousermvp.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.hlub.dev.demousermvp.R;

public class DialogHelper {


    public static void dialogLoading(ProgressDialog progressDialog, String message) {

        progressDialog.setTitle("Loading...");
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void dialogDelete(final Context context, final DialogCallback callback) {
        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.dialog_delete);

        TextView tvHuy = dialog.findViewById(R.id.tvDeleteHuy);
        TextView tvOk = dialog.findViewById(R.id.tvDeleteOK);

        tvHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onAgree();

            }
        });
        dialog.show();
    }
}
