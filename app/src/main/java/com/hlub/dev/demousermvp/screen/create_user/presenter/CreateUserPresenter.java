package com.hlub.dev.demousermvp.screen.create_user.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.screen.create_user.model.CreateUserInteractor;
import com.hlub.dev.demousermvp.screen.create_user.view.ICreateUserView;
import com.hlub.dev.demousermvp.utils.NetworkHelper;

import java.io.IOException;

import okhttp3.ResponseBody;

public class CreateUserPresenter {
    private CreateUserInteractor interactor = new CreateUserInteractor();
    private ICreateUserView view;

    public CreateUserPresenter(ICreateUserView view) {
        this.view = view;
    }

    public void createUser(ProgressDialog progressDialog, Context context, String name, String job) {

        if (!NetworkHelper.isCheckNetwork(context)) {
            view.onCreateUserError("Không có kết nối mạng");
            return;
        }

        if (name.equals("")) {
            view.onValidateName("Tên không được rỗng");
            return;
        }
        if (job.equals("")) {
            view.onValidateJob("Nghề nghiệp không được rỗng");
            return;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("job", job);

        //show loading
        view.onShowLoading("Please wait while creating...");

        interactor.createUser(jsonObject, new RetrofitCallback<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody obj) {
                view.onCloseLoading();
                view.onCreateUserSuccess();
            }

            @Override
            public void onError(String error) {
                view.onCloseLoading();
                view.onCreateUserError("Tạo user thất bại");
            }
        });
    }
}
