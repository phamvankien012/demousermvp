package com.hlub.dev.demousermvp.screen.list_user.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.entity.User;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.retrofit.UserRetrofit;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListUserInteractor {


    Call<User> call;

    public void getListUserByPage(int page, int perpage, final RetrofitCallback<User> listener) {
        call = UserRetrofit.getInstance().getListUserByPage(page, perpage);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(final Call<User> call, final Response<User> response) {
                listener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (call.isCanceled()) {
                    listener.onError("Request Cancel");
                } else {
                    listener.onError("Lấy User từ DB bị lỗi");
                }
            }
        });
    }

    public void cancelListUserByPage() {
        if (call != null) {
            call.cancel();
        }
    }

    public void deleteUser( final int id, final RetrofitCallback<User> listener) {

        UserRetrofit.getInstance().deleteUser(id).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                listener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                listener.onError("Lấy User bị lỗi");

            }
        });
    }



}
