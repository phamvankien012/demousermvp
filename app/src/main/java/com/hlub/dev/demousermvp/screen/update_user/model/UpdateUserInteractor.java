package com.hlub.dev.demousermvp.screen.update_user.model;

import android.app.ProgressDialog;
import android.content.Context;

import com.google.gson.JsonObject;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.retrofit.UserRetrofit;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;
import com.hlub.dev.demousermvp.utils.KeyboardHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserInteractor {
    public void updateUser(final int id, final JsonObject jsonObject, final RetrofitCallback<ResponseBody> listener) {


        UserRetrofit.getInstance().updateUser(id, jsonObject).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onError("Tạo user lỗi");

            }
        });

    }
}
