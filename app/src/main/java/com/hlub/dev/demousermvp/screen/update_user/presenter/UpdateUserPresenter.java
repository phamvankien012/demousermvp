package com.hlub.dev.demousermvp.screen.update_user.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.screen.update_user.model.UpdateUserInteractor;
import com.hlub.dev.demousermvp.screen.update_user.view.UpdateUserView;
import com.hlub.dev.demousermvp.utils.NetworkHelper;

import java.io.IOException;

import okhttp3.ResponseBody;

public class UpdateUserPresenter {

    private UpdateUserInteractor interactor = new UpdateUserInteractor();
    private UpdateUserView view;

    public UpdateUserPresenter(UpdateUserView view) {
        this.view = view;
    }

    public void updateUser(ProgressDialog progressDialog, Context context, int id, String name, String job) {
        if (!NetworkHelper.isCheckNetwork(context)) {
            view.onUpdateUserError("Không có kết nối mạng");
            return;
        }
        //check null
        if (name.equals("")) {
            view.onValidateName("Tên không được rỗng");
            return;
        }

        if (job.equals("")) {
            view.onValidateJob("Nghề nghiệp không được rỗng");
            return;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("job", job);

        //show loading
        view.onShowLoading("Please wait while updating...");

        interactor.updateUser(id, jsonObject, new RetrofitCallback<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody obj) {
                view.onCloseLoading();
                view.onUpdateUserSuccess();
            }

            @Override
            public void onError(String error) {
                view.onCloseLoading();
                view.onUpdateUserError("Cập nhật user thất bại");
            }
        });
    }
}
