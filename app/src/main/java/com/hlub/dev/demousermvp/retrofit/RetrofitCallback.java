package com.hlub.dev.demousermvp.retrofit;

public interface RetrofitCallback<T> {

    void onSuccess(T obj);

    void onError(String error);
}
