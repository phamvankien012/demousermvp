package com.hlub.dev.demousermvp.screen.create_user.view;

import android.content.Context;

public interface ICreateUserView {

    void onCreateUserSuccess();

    void onCreateUserError(String error);

    Context getContext();

    void onValidateName(String error);

    void onValidateJob(String error);

    void onShowLoading(String mess);

    void onCloseLoading();



}
