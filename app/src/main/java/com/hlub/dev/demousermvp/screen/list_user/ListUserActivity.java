package com.hlub.dev.demousermvp.screen.list_user;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.widget.ProgressBar;
import android.widget.Toast;

import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.adapter.UserAdapter;
import com.hlub.dev.demousermvp.base.BaseActivity;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.entity.User;
import com.hlub.dev.demousermvp.screen.create_user.CreateUserActivity;
import com.hlub.dev.demousermvp.screen.list_user.presenter.ListUserPresenter;
import com.hlub.dev.demousermvp.screen.list_user.view.ListUserView;
import com.hlub.dev.demousermvp.screen.update_user.UpdateUserActivity;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;
import com.hlub.dev.demousermvp.utils.EndlessRecyclerViewScrollListener;
import com.hlub.dev.demousermvp.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.List;

public class ListUserActivity extends BaseActivity implements ListUserView {
    private ImageView imgAddUser;
    private RecyclerView recycleView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ListUserPresenter presenter;
    private UserAdapter userAdapter;

    public static final int RES_UPDATE = 1;

    private int page = 1;
    private int per_page = 6;
    private EndlessRecyclerViewScrollListener scrollListener;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new ListUserPresenter(this);
        progressDialog = new ProgressDialog(this);
        initView();
        initRecycleView();
        initSwipeRefresh();
        initListener();
        getData();
        getDataOnLoadMore();

    }

    @Override
    protected void onPause() {
        super.onPause();
        progressDialog.dismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();
        progressDialog.dismiss();
    }

    private void initView() {
        recycleView = findViewById(R.id.recycleView);
        imgAddUser = findViewById(R.id.imgAddUser);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }


    private void initRecycleView() {
        recycleView.setLayoutManager(linearLayoutManager);
        userAdapter = new UserAdapter(this, this);
        recycleView.setAdapter(userAdapter);
    }

    private void initListener() {
        imgAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListUserActivity.this, CreateUserActivity.class));
            }
        });
    }

    private void initSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (!checkOnNetwork(getContext())) {
                    onNetworkNotConnected();
                } else {
                    presenter.cancelRequestServer();
                    userAdapter.clearData();
                    getData();
                }
            }
        });
    }

    private void getData() {
        userAdapter.setNetwork(checkOnNetwork(getContext()));
        page=1;
        presenter.getListUserByPage(page, per_page);
    }


    private void getDataOnLoadMore() {
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page, int totalItemsCount, RecyclerView view) {

//
//                                    new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        presenter.getListUserByPage(1, per_page);
//                    }
//                }, 5000);
                if(userAdapter.isLoadMore()){
                    presenter.getListUserByPage(page+1, per_page);

                }

            }

        };
        recycleView.addOnScrollListener(scrollListener);

    }

    /*
     *IListUserView
     */
    @Override
    public void onGetListDatumSuccess(final List<Datum> datumList) {
        Log.e("user", "onGetListDatumSuccess: " + datumList.size());
        swipeRefreshLayout.setRefreshing(false);
        if (datumList.size() == 0) {
            userAdapter.removeNull();
        } else {
            if (userAdapter.isDataEmpty()) {
                userAdapter.setListData(datumList);
                // recycleView.scheduleLayoutAnimation();
            } else {
                userAdapter.addData(datumList);
            }
        }
    }

    @Override
    public void onGetListUserError(String error) {
        swipeRefreshLayout.setRefreshing(false);
        showToastShort(error);
    }


    @Override
    public void onDeleteUserSuccess(final int position) {
        userAdapter.deleteUser(position);
        showToastShort("Xóa user thành công");
    }

    @Override
    public void onDeleteUserError(String error) {
        progressDialog.dismiss();
        showToastShort(error);
    }

    @Override
    public void onClickUpdateUser(int pos, Datum datum) {
        Intent intent = new Intent(ListUserActivity.this, UpdateUserActivity.class);
        intent.putExtra("pos", String.valueOf(pos));
        intent.putExtra("datum", datum);
        startActivityForResult(intent, RES_UPDATE);
    }


    @Override
    public void onNetworkNotConnected() {
        showToastShort("Không có kết nối mạng");
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onShowDialogDeleteUser(final int id, final int position) {
        DialogHelper.dialogDelete(ListUserActivity.this, new DialogCallback() {
            @Override
            public void onAgree() {
                presenter.deleteUser(id, position);
            }

            @Override
            public void disAgree() {

            }
        });
    }

    @Override
    public void onShowLoadingDeleteUser(String mess) {
        DialogHelper.dialogLoading(progressDialog, mess);
    }

    @Override
    public void onCloseLoadingDelete() {
        progressDialog.dismiss();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RES_UPDATE) {

            if (resultCode == RESULT_OK) {
                String pos = data.getStringExtra("pos");
                String name = data.getStringExtra("name");
                String email = data.getStringExtra("email");

                userAdapter.updateItem(Integer.parseInt(pos), name, email);

                showToastShort("Cập nhật thành công");

            }
        }

    }
}
