package com.hlub.dev.demousermvp.screen.list_user.view;

import android.content.Context;
import android.view.View;
import android.view.ViewParent;

import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.entity.User;

import java.util.List;

public interface ListUserView {

    void onGetListDatumSuccess(List<Datum> datumList);

    void onGetListUserError(String error);


    void onDeleteUserSuccess(int pos);

    void onDeleteUserError(String error);

    void onClickUpdateUser(int pos, Datum datum);

    void onNetworkNotConnected();

    Context getContext();

    void onShowDialogDeleteUser(final int id, final int position);

    void onShowLoadingDeleteUser(String mess);

    void onCloseLoadingDelete();


}
