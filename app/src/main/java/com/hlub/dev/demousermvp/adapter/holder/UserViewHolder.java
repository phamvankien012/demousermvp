package com.hlub.dev.demousermvp.adapter.holder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hlub.dev.demousermvp.R;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.screen.list_user.presenter.ListUserPresenter;
import com.hlub.dev.demousermvp.screen.list_user.view.ListUserView;
import com.hlub.dev.demousermvp.utils.DialogHelper;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView imgItemUser;
    private TextView tvItemName;
    private TextView tvItemEmail;
    private ImageView imgItemDeleteUser;
    private LinearLayout containerUpdate;


    private Context context;

    public UserViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        imgItemUser = itemView.findViewById(R.id.imgItemUser);
        tvItemName = itemView.findViewById(R.id.tvItemName);
        tvItemEmail = itemView.findViewById(R.id.tvItemEmail);
        imgItemDeleteUser = itemView.findViewById(R.id.imgItemDeleteUser);
        containerUpdate = itemView.findViewById(R.id.container_update);
        this.context = context;
    }

    public void bind(final ListUserView listUserView, final Datum datum, final int pos) {
        if (datum == null) {
            Toast.makeText(context, "NUll POIN", Toast.LENGTH_SHORT).show();
        }
        tvItemName.setText(datum.getFirstName() + " " + datum.getLastName());
        tvItemEmail.setText(datum.getEmail());
        Picasso.get()
                .load(datum.getAvatar()).resize(50, 50).centerCrop().into(imgItemUser);

        imgItemDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listUserView.onShowDialogDeleteUser(datum.getId(), pos);

            }
        });

        containerUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listUserView.onClickUpdateUser(pos, datum);

            }
        });
    }
}
