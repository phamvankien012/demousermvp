package com.hlub.dev.demousermvp.adapter.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NetworkViewHolder extends RecyclerView.ViewHolder {
    public NetworkViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
