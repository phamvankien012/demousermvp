package com.hlub.dev.demousermvp.retrofit;

import com.google.gson.JsonObject;
import com.hlub.dev.demousermvp.entity.Datum;
import com.hlub.dev.demousermvp.entity.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {

    @GET("/api/users")
    Call<User> getListUser();

    @GET("/api/users")
    Call<User> getListUserByPage(@Query("page") int page, @Query("per_page") int per_page);


    @DELETE("/api/users/{id}")
    Call<User> deleteUser(@Path("id") int id);


    @POST("/api/users")
    Call<ResponseBody> createUser(@Body JsonObject model);

    @PUT("/api/v1/update/{id}")
    Call<ResponseBody> updateUser(@Path("id") int itemId, @Body JsonObject jsonObject);
}
