package com.hlub.dev.demousermvp.screen.create_user.model;

import android.app.ProgressDialog;
import android.content.Context;

import com.google.gson.JsonObject;
import com.hlub.dev.demousermvp.retrofit.RetrofitCallback;
import com.hlub.dev.demousermvp.retrofit.UserRetrofit;
import com.hlub.dev.demousermvp.utils.DialogCallback;
import com.hlub.dev.demousermvp.utils.DialogHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserInteractor {

    public void createUser(final JsonObject jsonObject, final RetrofitCallback<ResponseBody> listener) {

        UserRetrofit.getInstance().createUser(jsonObject).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onError("Tạo user lỗi");

            }
        });


    }
}
